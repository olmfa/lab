'use strict';
$_$wp(1);
function isNumeric(n) {
    $_$wf(1);
    return $_$w(1, 0), ($_$w(1, 1), !isNaN(parseFloat(n))) && ($_$w(1, 2), isFinite(n));
}
function SearchProducts(products, search) {
    $_$wf(1);
    if ($_$w(1, 3), ($_$w(1, 4), Array.isArray(products)) && ($_$w(1, 5), typeof search == 'string')) {
        return $_$w(1, 6), products.filter(function (prod) {
            $_$wf(1);
            return $_$w(1, 7), ($_$w(1, 8), prod.name.toLowerCase().includes(search.toLowerCase())) || ($_$w(1, 9), prod.description.toLowerCase().includes(search.toLowerCase()));
        });
    }
}
function SortProducts(products, sortRule) {
    $_$wf(1);
    if ($_$w(1, 10), Array.isArray(products)) {
        return $_$w(1, 11), products.sort(function (a, b) {
            $_$wf(1);
            return $_$w(1, 12), a[sortRule] > b[sortRule] ? ($_$w(1, 13), 1) : ($_$w(1, 14), -1);
        });
    }
}
function Product(obj = {}) {
    $_$wf(1);
    $_$w(1, 15), this.ID = ($_$w(1, 16), obj.ID) || ($_$w(1, 17), '');
    $_$w(1, 18), this.name = ($_$w(1, 19), obj.name) || ($_$w(1, 20), '');
    $_$w(1, 21), this.description = ($_$w(1, 22), obj.description) || ($_$w(1, 23), '');
    $_$w(1, 24), this.price = ($_$w(1, 25), isNumeric(obj.price)) || ($_$w(1, 26), 0);
    $_$w(1, 27), this.brand = ($_$w(1, 28), obj.brand) || ($_$w(1, 29), '');
    $_$w(1, 30), this.sizes = ($_$w(1, 31), obj.sizes) || ($_$w(1, 32), []);
    $_$w(1, 33), this.activeSize = ($_$w(1, 34), obj.activeSize) || ($_$w(1, 35), '');
    $_$w(1, 36), this.quantity = ($_$w(1, 37), isNumeric(obj.quantity)) || ($_$w(1, 38), 0);
    $_$w(1, 39), this.date = ($_$w(1, 40), obj.date) || ($_$w(1, 41), new Date());
    $_$w(1, 42), this.reviews = ($_$w(1, 43), obj.reviews) || ($_$w(1, 44), []);
    $_$w(1, 45), this.images = ($_$w(1, 46), obj.images) || ($_$w(1, 47), []);
    $_$w(1, 48), this.getID = function () {
        $_$wf(1);
        return $_$w(1, 49), this.ID;
    };
    $_$w(1, 50), this.getName = function () {
        $_$wf(1);
        return $_$w(1, 51), this.name;
    };
    $_$w(1, 52), this.getDescription = function () {
        $_$wf(1);
        return $_$w(1, 53), this.description;
    };
    $_$w(1, 54), this.getPrice = function () {
        $_$wf(1);
        return $_$w(1, 55), this.price;
    };
    $_$w(1, 56), this.getBrand = function () {
        $_$wf(1);
        return $_$w(1, 57), this.brand;
    };
    $_$w(1, 58), this.getSizes = function () {
        $_$wf(1);
        return $_$w(1, 59), this.sizes;
    };
    $_$w(1, 60), this.getActiveSize = function () {
        $_$wf(1);
        return $_$w(1, 61), this.activeSize;
    };
    $_$w(1, 62), this.getQuanitity = function () {
        $_$wf(1);
        return $_$w(1, 63), this.quantity;
    };
    $_$w(1, 64), this.getDate = function () {
        $_$wf(1);
        return $_$w(1, 65), this.date;
    };
    $_$w(1, 66), this.getReviews = function () {
        $_$wf(1);
        return $_$w(1, 67), this.reviews;
    };
    $_$w(1, 68), this.getReviewByID = function (key) {
        $_$wf(1);
        return $_$w(1, 69), this.reviews.find(function (reviews) {
            $_$wf(1);
            return $_$w(1, 70), reviews.ID === key;
        });
    };
    $_$w(1, 71), this.getImages = function () {
        $_$wf(1);
        return $_$w(1, 72), this.images;
    };
    $_$w(1, 73), this.getImage = function (param) {
        $_$wf(1);
        return $_$w(1, 74), ($_$w(1, 75), this.images.find(function (imgName) {
            $_$wf(1);
            return $_$w(1, 77), imgName === param;
        })) || ($_$w(1, 76), this.images[0]);
    };
    $_$w(1, 78), this.setName = function (Name) {
        $_$wf(1);
        $_$w(1, 79), this.name = Name;
    };
    $_$w(1, 80), this.setDescription = function (descr) {
        $_$wf(1);
        $_$w(1, 81), this.description = descr;
    };
    $_$w(1, 82), this.setPrice = function (Price) {
        $_$wf(1);
        if ($_$w(1, 83), ($_$w(1, 84), Price > 0) && ($_$w(1, 85), isNumeric(Price))) {
            $_$w(1, 86), this.price = Price;
        }
    };
    $_$w(1, 87), this.setBrand = function (Brand) {
        $_$wf(1);
        $_$w(1, 88), this.brand = Brand;
    };
    $_$w(1, 89), this.addSize = function (size_) {
        $_$wf(1);
        $_$w(1, 90), this.sizes.push(size_);
    };
    $_$w(1, 91), this.deleteSize = function (key) {
        $_$wf(1);
        return $_$w(1, 92), this.sizes.splice(key, 1);
    };
    $_$w(1, 93), this.setActiveSize = function (actSize) {
        $_$wf(1);
        $_$w(1, 94), this.activeSize = actSize;
    };
    $_$w(1, 95), this.setDate = function (date_) {
        $_$wf(1);
        if ($_$w(1, 96), date_ == null) {
            $_$w(1, 97), this.date = new Date();
        } else {
            $_$w(1, 98), this.date = date_;
        }
    };
    $_$w(1, 99), this.addReview = function (review) {
        $_$wf(1);
        $_$w(1, 100), this.reviews.push(review);
    };
    $_$w(1, 101), this.deleteReview = function (key) {
        $_$wf(1);
        $_$w(1, 102), this.reviews.splice(key, 1);
    };
    $_$w(1, 103), this.getAverageRating = function () {
        $_$wf(1);
        var mas = ($_$w(1, 104), this.reviews.map(function (value) {
            $_$wf(1);
            return $_$w(1, 105), value.rating;
        }));
        const avg = ($_$w(1, 106), {
            service: 0,
            price: 0,
            value: 0,
            quality: 0
        });
        for (let v of ($_$w(1, 107), mas)) {
            $_$w(1, 108), avg.service += v.service / mas.length;
            $_$w(1, 109), avg.price += v.price / mas.length;
            $_$w(1, 110), avg.value += v.value / mas.length;
            $_$w(1, 111), avg.quality += v.quality / mas.length;
        }
        return $_$w(1, 112), avg;
    };
}
function Review(reviews = {}) {
    $_$wf(1);
    $_$w(1, 113), this.ID = ($_$w(1, 114), reviews.ID) || ($_$w(1, 115), '');
    $_$w(1, 116), this.author = ($_$w(1, 117), reviews.author) || ($_$w(1, 118), '');
    $_$w(1, 119), this.date = ($_$w(1, 120), reviews.date) || ($_$w(1, 121), new Date());
    $_$w(1, 122), this.comment = ($_$w(1, 123), reviews.comment) || ($_$w(1, 124), '');
    $_$w(1, 125), this.rating = ($_$w(1, 126), Array.isArray(reviews.rating)) || ($_$w(1, 127), [{
            service: 0,
            price: 0,
            value: 0,
            quality: 0
        }]);
}
var gh = ($_$w(1, 128), {
    ID: 1,
    name: 'b',
    description: 'desc',
    price: 15.04,
    brand: 'dd',
    sizes: [
        'XS',
        'S',
        'M',
        'L',
        'XL',
        'XXL'
    ],
    activeSize: 'M',
    quantity: 6,
    date: new Date(),
    reviews: [
        {
            ID: 67,
            author: 'Kris L',
            date: new Date(),
            comment: 'kkkkk',
            rating: {
                service: 5,
                price: 7,
                value: 3,
                quality: 6
            }
        },
        {
            ID: 1,
            rating: {
                service: 5,
                price: 3,
                value: 7,
                quality: 4
            }
        }
    ],
    images: [
        'foto/1',
        'foto/2',
        'foto/3'
    ]
});
var gg = ($_$w(1, 129), {
    ID: 7,
    name: 'c',
    description: 'desc',
    price: 17.04,
    brand: 'dd',
    sizes: [
        'XS',
        'S',
        'M',
        'L',
        'XL',
        'XXL'
    ],
    activeSize: 'M',
    quantity: 6,
    date: new Date(),
    reviews: [
        {
            ID: 27,
            author: 'Kris L',
            date: new Date(),
            comment: 'kkkkk',
            rating: {
                service: 51,
                price: 7,
                value: 3,
                quality: 6
            }
        },
        {
            ID: 11,
            rating: {
                service: 49,
                price: 3,
                value: 7,
                quality: 4
            }
        }
    ],
    images: [
        'foto/1',
        'foto/2',
        'foto/3'
    ]
});
var gb = ($_$w(1, 130), {
    ID: 77,
    name: 'a',
    description: 'desc',
    price: 15,
    brand: 'dd',
    sizes: [
        'XS',
        'S',
        'M',
        'L',
        'XL',
        'XXL'
    ],
    activeSize: 'M',
    quantity: 6,
    date: new Date(),
    reviews: [
        {
            ID: 37,
            author: 'Kris L',
            date: new Date(),
            comment: 'kkkkk',
            rating: {
                service: 5,
                price: 7,
                value: 3,
                quality: 6
            }
        },
        { ID: 16 }
    ],
    images: [
        'foto/1',
        'foto/2',
        'foto/3'
    ]
});
var p = ($_$w(1, 131), new Product(gh));
var p1 = ($_$w(1, 132), new Product(gg));
var p2 = ($_$w(1, 133), new Product(gb));
$_$w(1, 134), $_$tracer.log(SortProducts([
    p,
    p1,
    p2
], 'price'), 'SortProducts([\n    p,\n    p1,\n    p2\n], ...', 1, 134);
$_$w(1, 135), 1;
$_$wpe(1);