"use strict";

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function AbstractProduct(obj = {}) {
  if (this.constructor === AbstractProduct) {
    throw new Error("Cannot instantiate abstract class");
  }

  this.ID = String(obj.ID) || "";
  this.name = obj.name || "";
  this.description = obj.description || "";
  if (isNumeric(obj.price)) this.price = Number(obj.price) || 0.0;
  this.brand = obj.brand || "";
  this.images = obj.images || [];
  this.quantity = isNumeric(obj.quantity) || 0;
  this.date = obj.date || new Date();
  this.reviews = obj.reviews || [];
}
AbstractProduct.prototype.getID = function() {
  return this.ID;
};

AbstractProduct.prototype.getName = function() {
  return this.name;
};

AbstractProduct.prototype.getDescription = function() {
  return this.description;
};

AbstractProduct.prototype.getPrice = function() {
  return this.price;
};

AbstractProduct.prototype.getBrand = function() {
  return this.brand;
};

AbstractProduct.prototype.getQuanitity = function() {
  return this.quantity;
};

AbstractProduct.prototype.getDate = function() {
  return this.date;
};

AbstractProduct.prototype.getReviews = function() {
  return this.reviews;
};

AbstractProduct.prototype.getReviewByID = function(key) {
  return this.reviews.find(function(reviews) {
    return reviews.ID === key;
  });
};

AbstractProduct.prototype.getImages = function() {
  return this.images;
};

AbstractProduct.prototype.getImage = function(param) {
  return (
    this.images.find(function(imgName) {
      return imgName === param;
    }) || this.images[0]
  );
};

AbstractProduct.prototype.setName = function(Name) {
  this.name = Name;
};

AbstractProduct.prototype.setDescription = function(descr) {
  this.description = descr;
};

AbstractProduct.prototype.setPrice = function(Price) {
  if (Price > 0 && isNumeric(Price)) this.price = Price;
};

AbstractProduct.prototype.setBrand = function(Brand) {
  this.brand = Brand;
};
AbstractProduct.prototype.addReview = function(review) {
  this.reviews.push(new Review(review));
};

AbstractProduct.prototype.deleteReview = function(key) {
  if (key != null) {
    this.reviews.splice(
      this.reviews.findIndex(function(reviews) {
        return reviews.ID == key;
      }),
      1
    );
  }
};
AbstractProduct.prototype.getFullInformation = function() {
  var a = "";
  for (var key in this) {
    if (typeof this[key] != "function") {
      a += key.toString() + ": " + this[key].toString() + ";\n";
    }
  }
  return a;
};

AbstractProduct.prototype.getPriceforQuantity = function(number) {
  if (isNumeric(number))
    return "$" + Math.round(number * this.price * 100) / 100;
  else return "";
};

AbstractProduct.SearchProducts = function(products, search) {
  if (Array.isArray(products) && typeof search == "string")
    return products.filter(function(prod) {
      return (
        prod.name.toLowerCase().includes(search.toLowerCase()) ||
        prod.description.toLowerCase().includes(search.toLowerCase())
      );
    });
};

AbstractProduct.SortProducts = function(products, sortRule) {
  if (Array.isArray(products))
    return products.sort(function(a, b) {
      return a[sortRule] < b[sortRule] ? -1 : 1;
    });
};

AbstractProduct.prototype.getAverageRating = function() {
  const avg = {
    service: 0,
    price: 0,
    value: 0,
    quality: 0
  };

  var name_p = ["service", "price", "value", "quality"];

  for (let val of name_p) {
    avg[val] =
      Math.round(
        (this.reviews
          .map(function(value) {
            return value.rating;
          })
          .reduce(function(acumulator, curr) {
            return acumulator + curr[val];
          }, 0) /
          this.reviews.length) *
          100
      ) / 100;
  }
  return avg;
};

AbstractProduct.prototype.getter_seter = function(name_, value) {
  if (typeof name_ == "string") {
    if (value == null) {
      let a = "get" + name_[0].toUpperCase() + name_.slice(1);
      for (let key in this) {
        if (typeof this[key] == "function")
          if (key == a) {
            return this[key]();
          }
      }
    } else {
      if (name_ == "price" || name_ == "power") {
        if (isNumeric(+value) && value <= 0) {
          throw "Error: is not numeric or...";
        }
      }
      if (name_ == "warranty") {
        if (isNumeric(+value) && value <= 0 && value >= 10) {
          throw "Error: is not numeric or...";
        }
      }
      let a = "set" + name_[0].toUpperCase() + name_.slice(1);
      for (let key in this) {
        if (typeof this[key] == "function")
          if (key == a) {
            return this[key](value);
          }
      }
    }
  }
};

AbstractProduct.prototype.getProductTileHTML = function() {
  let div = document.createElement("div");
  div.className = "card col-2 m-5";

  let img = document.createElement("img");
  img.className = "card-img-top";
  img.src = this.getImage();
  img.alt = "product-name";
  div.appendChild(img);

  let div1 = document.createElement("div");
  div1.className = "card-body";
  div.appendChild(div1);

  let h5 = document.createElement("h5");
  h5.className = "card-title";
  h5.textContent = this.name;
  div1.appendChild(h5);

  let p_desc = document.createElement("p");
  p_desc.className = "card-text ";
  p_desc.textContent = this.description.substring(0,100);
  div1.appendChild(p_desc);

  let p_price = document.createElement("p");
  p_price.className = "card-text";
  p_price.textContent = "$" + this.price;
  div1.appendChild(p_price);

  let button = document.createElement("a");
  button.href = "#";
  button.className = "btn btn-outline-dark";
  button.textContent = "Buy";
  div1.appendChild(button);

  return div;
};

function Clothes(obj = {}) {
  AbstractProduct.apply(this, arguments);

  this.material = obj.material || "";
  this.color = obj.color || "";
  this.sizes = obj.sizes || [];
  this.activeSize = obj.activeSize || "";
}

Clothes.prototype = Object.create(AbstractProduct.prototype);
Clothes.prototype.constructor = AbstractProduct.constructor;

Clothes.prototype.getMaterial = function() {
  return this.material;
};

Clothes.prototype.getColor = function() {
  return this.color;
};

Clothes.prototype.getSizes = function() {
  return this.sizes;
};

Clothes.prototype.getActiveSize = function() {
  return this.activeSize;
};

Clothes.prototype.setMaterial = function(material_) {
  this.material = material_;
};

Clothes.prototype.setColor = function(color_) {
  return (this.color = color_);
};

Clothes.prototype.addSize = function(size_) {
  this.sizes.push(size_);
};

Clothes.prototype.deleteSize = function(size) {
  if (size != 0) {
    var a = 0;
    this.sizes.forEach(function(element, item) {
      if (element == size) {
        a = item;
      }
    });
    this.sizes.splice(a, 1);
  }
};

Clothes.prototype.setActiveSize = function(actSize) {
  this.activeSize = actSize;
};

function Electronic(obj = {}) {
  AbstractProduct.apply(this, arguments);

  if (isNumeric(obj.warranty) && obj.warranty >= 0 && obj.warranty <= 10)
    this.warranty = obj.warranty;
  else this.warranty = 0;

  if (isNumeric(obj.power) && obj.power >= 0) this.power = obj.power;
  else this.power = 0;
}
Electronic.prototype = Object.create(AbstractProduct.prototype);
Electronic.prototype.constructor = AbstractProduct.constructor;

Electronic.prototype.getWarranty = function() {
  return this.warranty;
};

Electronic.prototype.getPower = function() {
  return this.power;
};

Electronic.prototype.setWarranty = function(warranty_) {
  if (isNumeric(warranty_) && warranty_ >= 0 && warranty_ <= 10)
    this.warranty = warranty_;
};
AbstractProduct.prototype.setPrice = function(Price) {
  if (Price > 0 && isNumeric(Price)) this.price = Price;
};

Electronic.prototype.setPower = function(power_) {
  if (isNumeric(power_) && power_ >= 0) this.power = power_;
};

function Review(reviews = {}) {
  this.ID = reviews.ID || "";
  this.author = reviews.author || "";
  this.date = reviews.date || new Date();
  this.comment = reviews.comment || "";
  this.rating = {
    service: reviews.rating.service || 0,
    price: reviews.rating.price || 0,
    value: reviews.rating.value || 0,
    quality: reviews.rating.quality || 0
  };
}

var Validator = {
  validateEmail: function(param) {
    let regexp = /^((?![.-])[a-zA-Z0-9.-]{2,20})@([\w.!$%&'*+\/=\?^_-]{1,15})\.([a-zA-Z]{1,5}$)/;
    return regexp.test(param);
  },
  validatePhone: function(param) {
    let regexp = /^(?=^.{5,25}$)((([\+][0-9]{1,3})|(\-\-))?\s?(\(){0,1}(([\-\s]*[\d]){3})(\)){0,1}([\-\s]*[\d]){7})$/;
    return regexp.test(param);
  },
  validatePassword: function(param) {
    let regexp = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z_]{8,}$/;
    return regexp.test(param);
  },
  validateInput: function(param) {
    let regex_ = /^[a-zA-Z*]{3,30}$/;
    return regex_.test(param);
  }
};

var plp = (function(my) {
  my.renderProducts = function(products) {
    let prodall = document.getElementById("product-all");
    $("#product-all").empty();
    products.forEach(function(product) {
      prodall.appendChild(product.getProductTileHTML());
    });
  };

  my.getProductJSONData = function(cb) {
    var param = { meth: "GET", url: "product-feed.json" };
    // $.ajax(param).done(data =>{data=JSON.parse(JSON.stringify(data));
    $.ajax(param).done(cb);
  };

  return my;
})(plp || {});

var arr = [];
function app(data) {
  data.forEach(function(elem) {
    arr.push(new Clothes(elem));
  });
  plp.renderProducts(arr);
}

plp.getProductJSONData(app);

function formValid() {
  let input = document.getElementById("valid-input");
  let error = document.getElementById("error-c");
  if (!Validator.validateInput(input.value)) {
    error.innerText = "ERROR";
    input.classList.add("is-invalid");
  } else {
    input.classList.remove("is-invalid");
    error.innerText = "";
  }
}

let input = document.getElementById("valid-input");

input.addEventListener("input", formValid);

let form = document.getElementById("search_mini_form");

function searchP() {
  var res = AbstractProduct.SearchProducts(arr, input.value);
  return res;
}

function sortP() {
  var key = $("#select-box").val();
  var result;

  if (key == 0) {
    result =  searchP();
  } else if (key == 1) {
    result = AbstractProduct.SortProducts( searchP(), "price");
    
  } else if (key == 2) {
    result = AbstractProduct.SortProducts(searchP(), "price");
    result.reverse();
  } else if (key == 3) {
    result = AbstractProduct.SortProducts(searchP(), "name");
  } else if (key == 4) {
    result = AbstractProduct.SortProducts(searchP(), "name");
    result.reverse();
  }
  
  return result;
}

function render() {
  
  plp.renderProducts(sortP());
}

form.addEventListener("submit", function(event) {
  event.preventDefault();
  render();
});

$("#select-box").on("change", function(event) {
  event.preventDefault();
  render();
});
